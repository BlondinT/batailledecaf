﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BatailleDeCafé
{
    class Partie
    {
        private Joueur m_joueur;
        private Joueur m_serveur;
        private Carte m_carte;
        private Connecteur m_connecteur;
        private bool jouerParIA;
        private bool FINI;
        private int[] resultats;
        private int[] coupprecedent;
        private char parcelleprecedente;
        private int[] coupsserveur;
        private char parcelleserveur;

        public Partie(Joueur p_joueur,Joueur p_serveur, Carte p_carte, Connecteur p_connecteur)
        {
            m_joueur = p_joueur;
            m_serveur = p_serveur;
            m_carte = p_carte;
            m_connecteur = p_connecteur;
            FINI = false;
            resultats = new int[3];
            coupprecedent = new int[2];
            parcelleprecedente = '?';
            coupprecedent[0] = 11;
            coupprecedent[1] = 11;
            coupsserveur= new int[2];
            parcelleserveur = '?';
            coupsserveur[0] = 11;
            coupsserveur[1] = 11;
        }

        public void LancementDeLaPartie()
        {
            jouerParIA = true;
            bool serveurajouer = true;
            while (!FINI)
            {
                //Affichage des deux cartes
                int[] coupJoue=new int[2];
                if (serveurajouer == true)
                {
                    Etat firstsate = new Etat(m_carte, coupsserveur[0], coupsserveur[1], parcelleprecedente);
                    int[] resultatsminimax = minimax(firstsate,3, -9999, 9999, true);
                    coupprecedent[0] = resultatsminimax[0];
                    coupprecedent[1] = resultatsminimax[1];
                    string aEnvoyer;
                    aEnvoyer = resultatsminimax[1].ToString() + resultatsminimax[2].ToString();
                    Console.WriteLine("COUP ENVOYE"+aEnvoyer);
                    m_joueur.PoseGraine(resultatsminimax[1], resultatsminimax[2],1, m_carte);
                    m_connecteur.ElementAEnvoyer(aEnvoyer);
                    serveurajouer = false;
                }

                //Vérification du retour serveur INVA (mauvaise graine) ou VALI (bonne graine)
                char[] recu = m_connecteur.GetTableauDecoder();
                if (recu[0] == 'V'&&!jouerParIA)
                {
                    Console.WriteLine("Le coup est valide"); 
                    //Planter la graine
                    m_joueur.PoseGraine(coupJoue[0], coupJoue[1],1, m_carte);
                }
                else
                {
                    Console.WriteLine("Le coup n'est pas valide");
                    m_joueur.RetireGraine();
                }
                AffichageDesCartes();

                //Reçu de la case joué par le serveur (B:xy) ou FINI si la partie est terminée
                recu = m_connecteur.GetTableauDecoder();
                if (recu[0] == 'B')
                {
                    Console.WriteLine("Le serveur à joué en " + recu[2] + " | " + recu[3]);
                    coupsserveur[0] = Convert.ToInt32(recu[2]) - 48;
                    coupsserveur[1] = Convert.ToInt32(recu[3]) - 48;
                    parcelleserveur = m_carte.GetIdIlot(Convert.ToInt32(recu[2]) - 48, Convert.ToInt32(recu[3]) - 48);
                    m_serveur.PoseGraine(Convert.ToInt32(recu[2]) - 48, Convert.ToInt32(recu[3]) - 48,2, m_carte);
                    serveurajouer = true;
                }
                else
                    FINI = true;
                AffichageDesCartes();

                //Reçu ENCO si on peut encore jouer ou FINI sinon
                if (recu[4] == 'E')
                    FINI = false;
                else
                    FINI = true;

                //Si FINI reçu des scores (S:aa:bb)
                if (FINI)
                {
                    recu = m_connecteur.GetTableauDecoder();
                    Console.WriteLine(recu);
                }
            }
        }

        private void AffichageDesCartes()
        {
            m_carte.AfficheCarteLettre();
            Console.WriteLine("");
            m_carte.AfficheCarteGraine();
        }

        private int[] JoueurJoue()
        {
            int[] joue = new int[2];
            string aEnvoyer;
            Console.WriteLine("Indiquez la ligne puis la colonne sur laquelle vous voulez jouer : ");
            Console.ReadLine();
            try
            {
                joue[0] = int.Parse(Console.ReadLine());
                joue[1] = int.Parse(Console.ReadLine());
            }catch(Exception e)
            {

            }

            aEnvoyer = joue[0].ToString() + joue[1].ToString();

            m_connecteur.ElementAEnvoyer(aEnvoyer);
            return joue;
        }

        private int[] DecodageString(string p_chaine)
        {
            int[] emplacement = new int[2];
            emplacement[0] = Convert.ToInt32(p_chaine[0]);
            emplacement[1] = Convert.ToInt32(p_chaine[1]);
            return emplacement;
        }



        //Fonction de L'IA ??
        private int[] minimax(Etat staterecu,int depth, int alpha, int beta, bool maximizingPlayer)
        {
            //Console.WriteLine(depth.ToString());
            if (depth == 0 || FINI == true)
            {
                resultats[0] = staterecu.CalculScore(maximizingPlayer);
                //Console.WriteLine("Score"+resultats[0].ToString());
                return resultats;
            }
            if (maximizingPlayer == true)
            {
                int maxEval = -9999;
                //Console.WriteLine("Joueur");
                for(int x = 0; x < 10; x++)
                {
                    for(int y=0; y < 10; y++)
                    {
                        //Console.WriteLine('j'+x.ToString() + y.ToString());
                        if (m_joueur.VerificationPoseGraine(x, y, staterecu))
                        {
                            Etat stateenvoye = new Etat(staterecu);
                            stateenvoye.SetGraine(x, y, 1);
                            stateenvoye.setCoupPrecedent(x, y);
                            /*String jourge;
                            int[] coupserveur = stateenvoye.Coupsserveur;
                            Console.WriteLine("Dernier coup serveur: " + coupserveur[0].ToString() + coupserveur[1].ToString());
                            jourge ="Coup Joueur: "+x.ToString() + y.ToString();
                            Console.WriteLine(jourge);*/
                            stateenvoye.Parcelleprecedente = stateenvoye.GetIdIlot(x, y);
                            int[] eval = minimax(stateenvoye, depth - 1, alpha, beta, false);
                            if (maxEval <= eval[0])
                            {
                                resultats[1] = x;
                                resultats[2] = y;
                            }
                            maxEval = Math.Max(maxEval, eval[0]);
                            alpha = Math.Max(alpha, eval[0]);
                            if (beta <= alpha)
                            {
                                break;
                            }
                        }
                    }
                }
                resultats[0] = maxEval;
                return resultats;
            }
            else
            {
                int minEval = +9999;
                //Console.WriteLine("Serveur");
                for(int x = 0; x < 10; x++)
                {
                    for(int y = 0; y < 10; y++)
                    {
                        //Console.WriteLine('s'+x.ToString() + y.ToString());
                        if (m_serveur.VerificationPoseGraineServeur(x,y,staterecu))
                        {
                            Etat stateenvoye = new Etat(staterecu);
                            stateenvoye.SetGraine(x, y, 2);
                            stateenvoye.setCoupPrecedentServeur(x, y);
                            /*int[] coupjoueur = stateenvoye.Coupprecedent;
                            Console.WriteLine("Dernier coup joueur: " + coupjoueur[0].ToString() + coupjoueur[1].ToString());
                            String jourge;
                            jourge ="Coup serveur: "+ x.ToString()+y.ToString();
                            Console.WriteLine(jourge);*/
                            stateenvoye.Parcelleserveur = stateenvoye.GetIdIlot(x, y);
                            int[] eval = minimax(stateenvoye, depth - 1, alpha, beta, true);
                            minEval = Math.Min(minEval, eval[0]);
                            beta = Math.Min(beta, eval[0]);
                            if (beta <= alpha)
                            {
                                break;
                            }
                        }
                    }
                }
                resultats[0] = minEval;
                return resultats;
            }
        }
    }
}
