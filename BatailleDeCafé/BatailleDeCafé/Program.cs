using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleDeCafé
{
    public class Program
    {
        static void Main(string[] args)
        {
            //Console.SetBufferSize(Int16.MaxValue-1, Int16.MaxValue-1);
            Connecteur Cn = new Connecteur(127, 0, 0, 1, 1213);
            Cn.Recevoir();
            Cn.DecodageDeLaReception();
            Cn.toString();
            String Trame = Cn.GetTrame();
            Carte C = new Carte(Trame);

            Joueur joueur = new Joueur("Tanguy");
            Joueur serveur = new Joueur("Serveur");

            Partie partie = new Partie(joueur,serveur, C, Cn);
            partie.LancementDeLaPartie();

            Console.ReadKey();
        }
    }
}
