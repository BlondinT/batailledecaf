using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleDeCafé
{
    public class Parcelle
    {
        private int y;
        //Prend la lettre de l'ilôt auquel elle appartient ou alors M/F
        char id_ilot;
        //Frontières de l'ilôt
        int[] frontieres;
        private int x;
        private byte m_graine;

        public Parcelle()
        {
            this.x = -1;
            this.y = -1;
            this.frontieres = null;
            this.Id_ilot = '1';
            m_graine = 0;
        }
        public Parcelle(int x, int y, int[] frontieres)
        {
            this.x = x;
            this.y = y;
            this.frontieres = frontieres;
            this.Id_ilot = '1';
            m_graine = 0;
        }
        public Parcelle(int x, int y, int[] frontieres, char chardelacase)
        {
            this.x = x;
            this.y = y;
            this.frontieres = frontieres;
            this.Id_ilot = chardelacase;
            m_graine = 0;
        }
        public Parcelle(int x, int y, int[] frontieres, char chardelacase,byte m_graine)
        {
            this.x = x;
            this.y = y;
            this.frontieres = frontieres;
            this.Id_ilot = chardelacase;
            this.m_graine = m_graine;
        }

        //Getter et setter
        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }
        public char Id_ilot { get => id_ilot; set => id_ilot = value; }
        public int[] Frontieres { get => frontieres; set => frontieres = value; }

        public byte GetGraine()
        {
            return m_graine;
        }

        public void SetGraine(byte p_graine)
        {
            m_graine = p_graine;
        }
    }
}
