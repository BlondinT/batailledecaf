﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleDeCafé
{
    class Etat
    {
        private Parcelle[,] CarteParcelle;
        private char lettreMaximale;
        private int[] coupprecedent= new int[2];
        private char parcelleprecedente;
        private int[] coupsserveur=new int[2];
        private char parcelleserveur;

        public int[] Coupprecedent { get => coupprecedent; set => coupprecedent = value; }
        public char Parcelleprecedente { get => parcelleprecedente; set => parcelleprecedente = value; }
        public int[] Coupsserveur { get => coupsserveur; set => coupsserveur = value; }
        public char Parcelleserveur { get => parcelleserveur; set => parcelleserveur = value; }

        public void setCoupPrecedent(int x, int y)
        {
            this.coupprecedent[0] = x;
            this.coupprecedent[1] = y;
        }
        public void setCoupPrecedentServeur(int x, int y)
        {
            this.coupsserveur[0] = x;
            this.coupsserveur[1] = y;
        }
        public Etat(Carte carte, int coupsserveurX, int coupsserveurY ,char parcelleprecedente)
        {
            CarteParcelle = new Parcelle[10, 10];
            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    CarteParcelle[x, y] = new Parcelle(x, y, carte.CarteParcelle1[x, y].Frontieres, carte.CarteParcelle1[x, y].Id_ilot, carte.CarteParcelle1[x, y].GetGraine());
                }
            }
            this.coupsserveur[0] = coupsserveurX;
            this.coupsserveur[1] = coupsserveurY;
            this.parcelleprecedente = parcelleprecedente;
            lettreMaximale = carte.GetLettre();
        }
        public Etat(Etat state)
        {
            
            CarteParcelle = new Parcelle[10, 10];
            for(int x=0; x < 10; x++){
                for(int y = 0; y < 10; y++)
                {
                    CarteParcelle[x, y] = new Parcelle(x, y, state.CarteParcelle[x, y].Frontieres, state.CarteParcelle[x, y].Id_ilot, state.CarteParcelle[x, y].GetGraine());
                }
            }
            lettreMaximale = state.GetLettre();
            this.coupprecedent[0] = state.coupprecedent[0];
            this.coupprecedent[1] = state.coupprecedent[1];
            this.coupsserveur[0] = state.coupsserveur[0];
            this.coupsserveur[1] = state.coupsserveur[1];
            this.parcelleprecedente = state.parcelleprecedente;
            this.parcelleserveur = state.parcelleserveur;
        }
        public char GetIdIlot(int p_ligne, int p_colonne)
        {
            return CarteParcelle[p_ligne, p_colonne].Id_ilot;
        }

        public bool LaParcelleEstCultivable(int p_ligne, int p_colonne)
        {
            //On vérifie que la parcelle est cultivable
            if (CarteParcelle[p_ligne, p_colonne].Id_ilot == 'M' || CarteParcelle[p_ligne, p_colonne].Id_ilot == 'F')
                return false;

            return true;
        }

        public Parcelle[,] GetCarte()
        {
            return CarteParcelle;
        }

        public byte GetGraine(int p_ligne, int p_colonne)
        {
            return CarteParcelle[p_ligne, p_colonne].GetGraine();
        }

        public bool LaParcelleEstVierge(int p_ligne, int p_colonne)
        {
            //On vérifie que la parcelle ne contient pas de graine
            if (CarteParcelle[p_ligne, p_colonne].GetGraine() == 0)
                return true;
            return false;
        }

        public int RecuperationScore(char p_lettre, byte whosplaying)
        {
            int p_scores = 0;
            byte tailleParcelle = 0, nbGraineJoueur = 0;
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    if (CarteParcelle[ligne, colonne].Id_ilot == p_lettre)
                    {
                        tailleParcelle++;
                        if (CarteParcelle[ligne, colonne].GetGraine() == whosplaying)
                            nbGraineJoueur++;
                    }
                }
            }
            if (nbGraineJoueur > tailleParcelle / 2)
                p_scores += tailleParcelle;
            //if (whosplaying == 2) p_scores = 0-p_scores;
            Console.WriteLine(p_scores);
            return p_scores;
        }

        public int CalculScore(bool maximazingplayer)
        {
            int tailleTableau = lettreMaximale - 96;
            int[,] resultats = new int[3, tailleTableau];
            int score;


            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    if (GetIdIlot(ligne, colonne) != 'F' && GetIdIlot(ligne, colonne) != 'M')
                    {
                        resultats[0, GetIdIlot(ligne, colonne) - 97]++;
                        if (GetGraine(ligne, colonne) != 0)
                            resultats[GetGraine(ligne, colonne), GetIdIlot(ligne, colonne) - 97]++;
                    }
                }
            }

            int[] scores = new int[] { 0, 0 };

            for (int colonne = 0; colonne < tailleTableau; colonne++)
            {
                if (resultats[1, colonne] > resultats[2, colonne])
                    scores[0] += resultats[0, colonne];
                else if (resultats[1, colonne] < resultats[2, colonne])
                    scores[1] += resultats[0, colonne];
            }
            score = scores[0];
            //Console.WriteLine(score);
            return score;
        }

        public char GetLettre()
        {
            return lettreMaximale;
        }

        public void SetGraine(int p_ligne, int p_colonne, byte p_joueur)
        {
            CarteParcelle[p_ligne, p_colonne].SetGraine(p_joueur);
        }
        public Etat ShallowCopy()
        {
            return (Etat)this.MemberwiseClone();
        }
    }
}
