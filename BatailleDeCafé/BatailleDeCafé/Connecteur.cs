﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace BatailleDeCafé
{
    public class Connecteur
    {
        /* ----- Declaration des variables ----- */


        //Tableau qui recevera tout les bits brut du serveurs 
        private byte[] TableauDeReception;
        //Tableau qui contient ce qui sera envoyer au serveur 
        private byte[] TableauDEnvoie;
        //Tableau qui contiendra l'adresse du serveur (Initialisé avec 4bit de base) 
        private byte[] AdresseDuServeur = new byte[4];
        //Tableau qui continedra la trame final, une fois passer par la case décodage 
        private char[] TableauDecoder;
        //Port sur lequel doit se connecter le connecteur  
        private int Port;
        //Trame final, décodée 
        private String Trame;


        /* -----Instanciation des objets ----- */


        //Création de la socket utilisée 
        private Socket Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);


        /* ----- Constructeur ----- */
        public Connecteur(byte PremierBit, byte Deuxiemebit, byte Troisiemebit, byte Quatriemebit, int Port)
        {
            /* -----Instanciation de l'adressse du serveur----- */
            AdresseDuServeur[0] = PremierBit;
            AdresseDuServeur[1] = Deuxiemebit;
            AdresseDuServeur[2] = Troisiemebit;
            AdresseDuServeur[3] = Quatriemebit;
            IPAddress IpServeur = new IPAddress(AdresseDuServeur);
            /* -----Instanciation Du port ----- */
            this.Port = Port;
            try
            {
                Socket.Connect(IpServeur, Port); //Connexion au serveur grace a son addresse et son port 
            }
            catch (Exception)
            {
                Console.WriteLine("Error 0 : Failed to connect to servor"); //Si la connexion n'est pas reussi, cela lève une exception 
            }
        }


        /* ----- Reception des données depuis le serveur ----- */
        public void Recevoir()
        {
            try
            {
                TableauDeReception = new byte[1000]; //Instanciation d'un tableau surdimensonné 
                Socket.Receive(TableauDeReception); //Reception de ce tableau 
            }
            catch (Exception)
            {
                Console.WriteLine("Error 1 : Failed to receive informartions"); //Si il ne parviens pas a recuperer des informations, une exception est levée 
            }
        }

        /* ----- Envoie des infos au serveur ----- */
        public void ElementAEnvoyer(int p_ligne, int p_colonne)
        {
            string stringAEnvoyer = "A:" + p_ligne.ToString() + p_colonne.ToString();
            byte[] chaineEncodee = this.Encodage(stringAEnvoyer);

            Socket.Send(chaineEncodee);
        }

        public void ElementAEnvoyer(string p_chaineAEnvoyer)
        {
            string chaineAEnvoyer = "A:" + p_chaineAEnvoyer;
            byte[] chaineEncodee = Encodage(chaineAEnvoyer);
            Socket.Send(chaineEncodee);
        }

        /* ----- Fonction d'encodage d'une chaine de caractère ----- */
        private byte[] Encodage(string p_chaineAEnvoyer)
        {
            Encoding ascii = Encoding.ASCII;
            byte[] chaineEncodee = ascii.GetBytes(p_chaineAEnvoyer);

            return chaineEncodee;
        }

        /* ----- Envoie des infos au serveur ----- */
        /* Ca va pas ça
        public void Envoie(int CoorX, int CoorY)
        {
            try
            {
                TableauDEnvoie = new byte[10];
                String Buffer = "A:" + CoorX + CoorY;
                for (int i = 0; i < Buffer.Length; i++)
                {
                    TableauDEnvoie[i] = Convert.ToByte(Buffer[i]);
                    Console.WriteLine(TableauDEnvoie[i]);
                }
            }
            catch (Exception)
            {

            }
        }
        */

        /* ----- Decodage du tableau recu précedement ----- */
        public void DecodageDeLaReception()
        {
            TableauDecoder = new char[1000]; //Instanciation d'un tableau surdimensioné (de même taille que celui de la reception pour eviter les soucis)
            try
            {
                for (int CompteurI = 0; CompteurI <= 999; CompteurI++)
                {
                    TableauDecoder[CompteurI] = Convert.ToChar(TableauDeReception[CompteurI]); //On passe sur toutes les cases du tableau est on le convertit en caractère 
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Error 2 : Failed to uncode the reception"); //Si une execption est lever, elle est gerée 
            }
        }


        /* ----- Exctinction et fermeture de la socket ----- */
        public void EteindreEtFermer()
        {
            try
            {
                Socket.Shutdown(SocketShutdown.Both); //Exctinction des deux sockets (celle du serveur et celle du programme)
                Socket.Close(); //Fermeture de la socket 
            }
            catch (Exception)
            {
                Console.WriteLine("Error 3 : Failed to shutdown"); //Si une execption est levée, elle est gérée 
            }
        }


        /* ----- Converti le tableu de char decoder en un String ----- */
        public void toString()
        {
            int compteur = 0;
            while (TableauDecoder[compteur] != '\0')
            {
                Trame += TableauDecoder[compteur];
                compteur++;
            }
        }

        /* ----- Accesseur ----- */
        public byte[] GetIPAdress()
        {
            return AdresseDuServeur;
        }
        public byte[] GetTableauDeReception()
        {
            return TableauDeReception;
        }
        public char[] GetTableauDecoder()
        {
            Recevoir();
            DecodageDeLaReception();
            return TableauDecoder;
        }
        public String GetTrame()
        {
            return this.Trame;
        }
    }


}
