﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleDeCafé
{
    class Joueur
    {
        private uint v_NombreGraine;
        private int v_Score;
        private string v_Nom = "Default";
        private int[] m_coupPrecedent = new int[2];
        private char m_parcelleCoupPrecedent;

        public Joueur(string p_Nom)
        {
            this.v_NombreGraine = 28;
            this.v_Score = 0;
            try
            {
                this.v_Nom = p_Nom;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            //Initialisation à 11 pour dire que le joueur n'a pas encore joué (50 étant hors du tableau de jeu)
            m_coupPrecedent[0] = 11;
            m_coupPrecedent[1] = 11;
            m_parcelleCoupPrecedent = '?';
        }

        public uint V_NombreGraine { get => v_NombreGraine; set => v_NombreGraine = value; }

        public bool VerificationPoseGraineServeur(int CoorX, int CoorY, Etat p_state)
        {
            int[] coupprecedent = p_state.Coupprecedent;
            char parcelleprecedente = p_state.Parcelleprecedente;
            if (!(CoorX==coupprecedent[0]&&CoorY==coupprecedent[1]))
            {
                //Console.WriteLine("Case diférente");
                if (p_state.LaParcelleEstVierge(CoorX, CoorY))
                {
                    //Console.WriteLine("Vierge");
                    if (p_state.LaParcelleEstCultivable(CoorX, CoorY))
                    {
                        //Console.WriteLine("Cultivable");
                        if (LalignementEstBon(CoorX, CoorY,coupprecedent))
                        {
                            //Console.WriteLine("Alignée");
                            if (LaParcelleEstDifferente(CoorX, CoorY, p_state,parcelleprecedente))
                            {
                                //Console.WriteLine("Parcelle Différente");
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        public bool VerificationPoseGraine(int CoorX, int CoorY, Etat p_state)
        {
            int[] coupsserveur = p_state.Coupsserveur;
            char parcelleprecedente = p_state.Parcelleserveur;
            if (!(CoorX == coupsserveur[0] && CoorY == coupsserveur[1]))
            {
                //Console.WriteLine("Case diférente");
                if (p_state.LaParcelleEstVierge(CoorX, CoorY))
                {
                    //Console.WriteLine("Vierge");
                    if (p_state.LaParcelleEstCultivable(CoorX, CoorY))
                    {
                       // Console.WriteLine("Cultivable");
                        if (LalignementEstBon(CoorX, CoorY, coupsserveur))
                        {
                            //Console.WriteLine("Alignée");
                            if (LaParcelleEstDifferente(CoorX, CoorY, p_state, parcelleprecedente))
                            {
                               // Console.WriteLine("Parcelle Différente");
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool LaParcelleEstDifferente(int p_ligne, int p_colonne, Etat p_state, char parcellecoupprecedent)
        {
            if (parcellecoupprecedent == '?')
                return true;

            if (p_state.GetIdIlot(p_ligne, p_colonne) != parcellecoupprecedent)
                return true;

            return false;
        }

        public void PoseGraine(int p_ligne, int p_colonne,byte kikijoue, Carte p_carte)
        {
            p_carte.SetGraine(p_ligne, p_colonne, kikijoue);
            m_coupPrecedent[0] = p_ligne;
            m_coupPrecedent[1] = p_colonne;
            v_NombreGraine--;
        }

        public void RetireGraine()
        {
            v_NombreGraine--;
        }

        public bool LalignementEstBon(int p_ligne, int p_colonne,int[] coupprecedent)
        {
            //Si le joueur n'a pas encore joué
            if (coupprecedent[0] == 11 && coupprecedent[1] == 11)
            {
                return true;
            }

            //Si le joueur place la graine sur la même ligne ou colonne que son coup précédent
            if (p_ligne == coupprecedent[0] || p_colonne == coupprecedent[1])
            {
                return true;
            }

            return false;
        }

        public byte CalculScoreCoup(int p_ligne, int p_colonne, Carte p_carte)
        {
            char ilotVise = p_carte.GetIdIlot(p_ligne, p_colonne);
            byte tailleParcelle = 0, nbGraineJoueur = 0, nbGraineAdversaire = 0;

            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    if (p_carte.GetIdIlot(ligne, colonne) == ilotVise)
                    {
                        tailleParcelle++;
                        if (p_carte.GetGraine(ligne, colonne) == 1)
                            nbGraineJoueur++;
                        else if (p_carte.GetGraine(ligne, colonne) == 2)
                            nbGraineAdversaire++;
                    }
                }
            }

            //Comme la graine n'a pas encore été posée alors il faut vérifier s'il y a égalité
            //et seulement à ce moment là ça veut dire que le coup nous apporte quelque chose
            if (nbGraineAdversaire == nbGraineJoueur)
                return tailleParcelle;

            /*if (nbGraineAdversaire > nbGraineJoueur)
                return 0;
            if (nbGraineAdversaire < nbGraineJoueur)
                return 0;*/

            return 0;
        }


        public void SetScore(Carte p_carte)
        {
            v_Score = 0;
            for (char car = 'a'; car < p_carte.GetLettre(); car++)
            {
                v_Score = p_carte.RecuperationScore(car, 1);
            }
        }

        public int GetScore()
        {
            return v_Score;
        }
        public void SetCoupPrecedent(int posx, int posy)
        {
            this.m_coupPrecedent[0] = posx;
            this.m_coupPrecedent[1] = posy;
        }
        public void Setparcellecoupprecedent(char data)
        {
            this.m_parcelleCoupPrecedent = data;
        }
    }
}
