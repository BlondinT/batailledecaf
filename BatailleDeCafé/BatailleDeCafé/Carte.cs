﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleDeCafé
{
    class Carte
    {
        private Parcelle[,] CarteParcelle;
        List<Ilot> ListeIlot;
        private char lettreMaximale;

        internal Parcelle[,] CarteParcelle1 { get => CarteParcelle; set => CarteParcelle = value; }

        public Carte(string Trame)
        {
            //Constructeur de la carte
            //Création d'un tableau / carte de 100 cases
            CarteParcelle = new Parcelle[10, 10];

            //On divise la trame pour récupérer les lignes de la carte dans un tableau
            String[] TabLigne = Trame.Split('|');
            for (int i = 0; i < TabLigne.Length - 1; i++)
            {
                //On récupère les numéros pour affectuer les bonnes frontières aux bonnes cases
                String[] colonestring = TabLigne[i].Split(':');
                for (int y = 0; y < colonestring.Length; y++)
                {
                    int num2decrypt = Int32.Parse(colonestring[y]);

                    if (num2decrypt > 30)
                    {
                        //Verification si c'est une case mer et affectation de son ID directement pour faciliter la suite
                        if (num2decrypt > 60)
                        {
                            int[] tabfrontiere = NumtoTabcaseautour(num2decrypt - 60);
                            CarteParcelle[i, y] = new Parcelle(i, y, tabfrontiere, 'M');
                        }
                        //Sinon case forêt
                        else
                        {
                            int[] tabfrontiere = NumtoTabcaseautour(num2decrypt - 30);
                            CarteParcelle[i, y] = new Parcelle(i, y, tabfrontiere, 'F');
                        }
                    }
                    //Affectation des id (a, b, c,...) aux ilôt
                    else
                    {
                        int[] tabfrontiere = NumtoTabcaseautour(num2decrypt);
                        CarteParcelle[i, y] = new Parcelle(i, y, tabfrontiere);
                    }
                }
            }
            ApplicationIlot(CarteParcelle);
        }

        private void ApplicationIlot(Parcelle[,] carteParcelle)
        {
            //On effectue une affectation par ligne

            //Lettre a affecté aux ilôt
            char derniereLettre = 'a';
            //Sert à éviter overwrite l'id_ilot d'une parcelle en cas d'exception / de cas particulier 
            bool DoesExceptionAppen = true;
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    //Affecte la première case de la carte
                    if (ligne == 0 && colonne == 0 && !(carteParcelle[ligne, colonne].Id_ilot != 'M' && carteParcelle[ligne, colonne].Id_ilot != 'F'))
                    {
                        derniereLettre = '`';
                    }
                    if (ligne == 0 && colonne == 0 && carteParcelle[ligne, colonne].Id_ilot != 'M' && carteParcelle[ligne, colonne].Id_ilot != 'F')
                    {
                        carteParcelle[ligne, colonne].Id_ilot = derniereLettre;
                    }
                    else if (carteParcelle[ligne, colonne].Id_ilot != 'M' && carteParcelle[ligne, colonne].Id_ilot != 'F')
                    {
                        if (carteParcelle[ligne, colonne].Frontieres[0] == 1 && carteParcelle[ligne, colonne].Frontieres[3] == 0 && carteParcelle[ligne, colonne].Frontieres[1] == 1 && carteParcelle[ligne, colonne].Frontieres[2] == 1)
                        {
                            //Nous indique si on est sûr que la case porte bien le l'id qu'on li a accorder
                            bool SUR = false;
                            int indexcolonne = 1;
                            while (!SUR)
                            {
                                if (carteParcelle[ligne, colonne + indexcolonne].Frontieres[0] == 1)
                                {
                                    if (carteParcelle[ligne, colonne + indexcolonne].Frontieres[3] == 1)
                                    {
                                        SUR = true;
                                    }
                                    else
                                    {
                                        indexcolonne += 1;
                                        if (colonne + indexcolonne >= 9)
                                        {
                                            SUR = true;
                                        }
                                    }
                                }
                                else
                                {
                                    carteParcelle[ligne, colonne].Id_ilot = carteParcelle[ligne - 1, colonne + indexcolonne].Id_ilot;
                                    SUR = true;
                                    DoesExceptionAppen = false;
                                }
                            }
                        }
                        if (carteParcelle[ligne, colonne].Frontieres[0] == 0)
                        {
                            carteParcelle[ligne, colonne].Id_ilot = carteParcelle[ligne - 1, colonne].Id_ilot;
                        }
                        else if (carteParcelle[ligne, colonne].Frontieres[1] == 0)
                        {
                            carteParcelle[ligne, colonne].Id_ilot = carteParcelle[ligne, colonne - 1].Id_ilot;
                        }
                        //Si on a gérer un exception
                        else if (DoesExceptionAppen)
                        {
                            derniereLettre++;
                            carteParcelle[ligne, colonne].Id_ilot = derniereLettre;
                        }
                        DoesExceptionAppen = true;
                    }
                }
            }
            lettreMaximale = derniereLettre;
        }

        private int[] NumtoTabcaseautour(int numtodecrypt)
        {
            int[] tabretour = { 0, 0, 0, 0 };

            //Mise en place des frontière grâce aux multiple de 2
            if (numtodecrypt == 8 || numtodecrypt / 8 == 1)
            {
                numtodecrypt = numtodecrypt % 8;
                tabretour[3] = 1;
            }
            if (numtodecrypt == 4 || numtodecrypt / 4 == 1)
            {
                numtodecrypt = numtodecrypt % 4;
                tabretour[2] = 1;
            }
            if (numtodecrypt == 2 || numtodecrypt / 2 == 1)
            {
                numtodecrypt = numtodecrypt % 2;
                tabretour[1] = 1;
            }
            if (numtodecrypt == 1)
            {
                tabretour[0] = 1;
            }
            return tabretour;
        }

        public void AfficheCarteLettre()
        {
            //Affiche la carte avec les id_ilot à leur emplacement
            for (int i = 0; i < 10; i++)
            {
                for (int y = 0; y < 10; y++)
                {
                    Console.Write(CarteParcelle[i, y].Id_ilot + " ");
                }
                Console.WriteLine("");
            }
        }

        public void AfficheCarteGraine()
        {
            for(int ligne = 0; ligne < 10; ligne++)
            {
                for(int colonne = 0; colonne < 10; colonne++)
                {
                    Console.Write(CarteParcelle[ligne, colonne].GetGraine() + " ");
                }
                Console.WriteLine("");
            }
        }


        public char GetIdIlot(int p_ligne, int p_colonne)
        {
            return CarteParcelle[p_ligne, p_colonne].Id_ilot;
        }

        public bool LaParcelleEstCultivable(int p_ligne, int p_colonne)
        {
            //On vérifie que la parcelle est cultivable
            if (CarteParcelle[p_ligne, p_colonne].Id_ilot == 'M' || CarteParcelle[p_ligne, p_colonne].Id_ilot == 'F')
                return false;

            return true;
        }

        public Parcelle[,] GetCarte()
        {
            return CarteParcelle;
        }

        public byte GetGraine(int p_ligne, int p_colonne)
        {
            return CarteParcelle[p_ligne, p_colonne].GetGraine();
        }

        public bool LaParcelleEstVierge(int p_ligne, int p_colonne)
        {
            //On vérifie que la parcelle ne contient pas de graine
            if (CarteParcelle[p_ligne, p_colonne].GetGraine() != 0)
                return false;

            return true;
        }

        public int RecuperationScore(char p_lettre ,byte whosplaying)
        {
            int p_scores = 0;
            byte tailleParcelle = 0, nbGraineJoueur = 0;
            for(int ligne = 0; ligne < 10; ligne++)
            {
                for(int colonne = 0; colonne < 10; colonne++)
                {
                    if(CarteParcelle[ligne, colonne].Id_ilot == p_lettre)
                    {
                        tailleParcelle++;
                        if (CarteParcelle[ligne, colonne].GetGraine() == whosplaying)
                            nbGraineJoueur++;
                    }
                }
            }

            if (nbGraineJoueur > tailleParcelle / 2)
                p_scores += tailleParcelle;
            //if (whosplaying == 2) p_scores = 0-p_scores;
            return p_scores;
        }

        public char GetLettre()
        {
            return lettreMaximale;
        }

        public void SetGraine(int p_ligne, int p_colonne, byte p_joueur)
        {
            CarteParcelle[p_ligne, p_colonne].SetGraine(p_joueur);
        }
    }
}
